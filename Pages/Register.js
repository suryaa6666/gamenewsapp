import React, { useState } from "react";
import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView, ToastAndroid } from "react-native";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import TextBox from 'react-native-password-eye';

export default function Register({ navigation }) {

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const showToast = ({ sentence }) => {
		ToastAndroid.show(sentence, ToastAndroid.SHORT);
	};

	const submit = () => {
		const Data = {
			email,
			password,
		};
		const auth = getAuth();
		createUserWithEmailAndPassword(auth, email, password)
			.then((userCredential) => {
				// Signed in
				const user = userCredential.user;
				showToast({ sentence: "Berhasil membuat akun!" });
				navigation.navigate("Login");
				console.log(user);
			})
			.catch((error) => {
				showToast({ sentence: "Gagal membuat akun!" });
				const errorCode = error.code;
				const errorMessage = error.message;
				console.error(errorCode, errorMessage);
			});
	};

	return (
		<View style={styles.container}>
			<ScrollView>
				<Image
					style={{ height: 115, width: 224, marginTop: 75, alignSelf: 'center' }}
					source={require("../assets/gamenewslogo.png")}
				/>
				<Text style={{ fontSize: 24, fontWeight: 'bold', marginTop: 10, marginBottom: 10 }}>
					Daftar
				</Text>
				<View>
					<TextBox
						style={{
							borderWidth: 1,
							paddingVertical: 10,
							borderRadius: 5,
							width: 300,
							marginBottom: 10,
							paddingHorizontal: 10,
							flexDirection: "row"
						}}
						placeholder="Masukan email"
						value={email}
						onChangeText={(value) => setEmail(value)}
					/>
					<TextBox
						style={{
							borderWidth: 1,
							paddingVertical: 10,
							borderRadius: 5,
							width: 300,
							marginBottom: 10,
							paddingHorizontal: 10,
							flexDirection: "row"
						}}
						placeholder="Masukan Password"
						value={password}
						onChangeText={(value) => setPassword(value)}
						secureTextEntry={true}
					/>
					<TouchableOpacity onPress={submit} style={{ flex: 1, backgroundColor: 'black', padding: 10, borderRadius: 5, borderColor: '#8E8383', borderWidth: 0.6, marginTop: 10 }}>
						<Text style={{ textAlign: 'center', color: 'white', fontSize: 18 }}> Daftar </Text>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => navigation.navigate("Login")} style={{ flex: 1, backgroundColor: 'white', padding: 10, borderRadius: 5, borderColor: '#8E8383', borderWidth: 0.6, marginTop: 10 }}>
						<Text style={{ textAlign: 'center', color: 'black', fontSize: 18 }}> Masuk </Text>
					</TouchableOpacity>
					<View style={{ marginTop: 20, marginBottom: 100, alignItems: 'center', justifyContent: 'center' }}>
						<Text style={[{ fontSize: 24, fontWeight: 'bold' }, styles.myfonts]}> Powered By : </Text>
						<Image
							style={{ height: 50, width: 200, marginTop: 4 }}
							source={require("../assets/thelazymedialogo.png")}
						/>
					</View>
				</View>
			</ScrollView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
		justifyContent: "center",
		alignItems: "center",
	},
});
