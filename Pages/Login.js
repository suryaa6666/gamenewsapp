import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import React, { useState } from "react";
import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView, ToastAndroid } from "react-native";
import TextBox from 'react-native-password-eye';

export default function Login({ navigation }) {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const showToast = ({ sentence }) => {
		ToastAndroid.show(sentence, ToastAndroid.SHORT);
	};

	const submit = () => {
		const auth = getAuth();
		signInWithEmailAndPassword(auth, email, password)
			.then((userCredential) => {
				// Signed in
				showToast({ sentence: "Berhasil login!" });
				const user = userCredential.user;
				navigation.navigate("Home", { email });
			})
			.catch((error) => {
				showToast({ sentence: "Gagal login, email / password salah!" });
				const errorCode = error.code;
				const errorMessage = error.message;
				console.error(errorCode, errorMessage);
			});
	};

	return (
		<View style={styles.container}>
			<ScrollView>
				<Image
					style={{ height: 115, width: 224, marginTop: 75, alignSelf: 'center' }}
					source={require("../assets/gamenewslogo.png")}
				/>
				<Text style={{ fontSize: 24, fontWeight: 'bold', marginTop: 10, marginBottom: 10 }}>
					Masuk
				</Text>
				<View>
					<TextBox
						style={{
							borderWidth: 1,
							paddingVertical: 10,
							borderRadius: 5,
							width: 300,
							marginBottom: 10,
							paddingHorizontal: 10,
						}}
						placeholder="Masukan Email"
						value={email}
						onChangeText={(value) => setEmail(value)}
					/>
					<TextBox
						style={{
							borderWidth: 1,
							paddingVertical: 10,
							borderRadius: 5,
							width: 300,
							marginBottom: 10,
							paddingHorizontal: 10,
							flexDirection: 'row'
						}}
						placeholder="Masukan Password"
						value={password}
						onChangeText={(value) => setPassword(value)}
						secureTextEntry={true}
					/>
					<TouchableOpacity onPress={submit} style={{ flex: 1, backgroundColor: 'black', padding: 10, borderRadius: 5, borderColor: '#8E8383', borderWidth: 0.6, marginTop: 10 }}>
						<Text style={{ textAlign: 'center', color: 'white', fontSize: 18 }}> Masuk </Text>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => navigation.navigate("Register")} style={{ flex: 1, backgroundColor: 'white', padding: 10, borderRadius: 5, borderColor: '#8E8383', borderWidth: 0.6, marginTop: 10 }}>
						<Text style={{ textAlign: 'center', color: 'black', fontSize: 18 }}> Daftar </Text>
					</TouchableOpacity>
					<View style={{ marginTop: 20, marginBottom: 100, alignItems: 'center', justifyContent: 'center' }}>
						<Text style={[{ fontSize: 24, fontWeight: 'bold' }, styles.myfonts]}> Powered By : </Text>
						<Image
							style={{ height: 50, width: 200, marginTop: 4 }}
							source={require("../assets/thelazymedialogo.png")}
						/>
					</View>
				</View>
			</ScrollView>
		</View >
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
		alignItems: "center",
	},
});
