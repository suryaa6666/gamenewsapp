import Axios from 'axios';
import React, { useState, useEffect } from "react";
import {
	StyleSheet,
	Text,
	View,
	Image,
	ScrollView,
	TouchableOpacity,
	ActivityIndicator
} from "react-native";

export default function Console({ route, navigation }) {

	const [isLoading, setLoading] = useState(true);
	const [data, setData] = useState([]);

	useEffect(() => {
		Axios.get('https://the-lazy-media-api.vercel.app/api/games/console-game?page=1')
			.then(({ data }) => {
				setData(data);
			})
			.catch((error) => console.error(error))
			.finally(() => setLoading(false));
	}, []);

	const RenderItem = ({ item }) => (
		<TouchableOpacity key={item.key} onPress={() => navigation.navigate("Detail", { key: item.key })} style={{ flex: 1, padding: 10, borderRadius: 5, borderColor: '#8E8383', borderWidth: 0.6, marginTop: 10, flexDirection: 'row' }}>
			<Image source={{ uri: item.thumb }} style={{ marginLeft: 18, alignSelf: "center", width: 125, height: 125, resizeMode: 'contain', marginTop: 35 }} />
			<View style={{ flex: 0.7, marginLeft: 30, alignItems: 'center', justifyContent: 'center' }}>
				<Text style={{ textAlign: 'left', color: 'black', fontSize: 18, fontWeight: '500' }}>{item.title}</Text>
			</View>
		</TouchableOpacity>
	);

	return (
		<View style={styles.container} >
			<ScrollView>
				<View style={{ backgroundColor: 'black', padding: 50, width: '100%' }}>
					<Text style={{ color: 'white', fontSize: 34, fontWeight: 'bold' }}>Berita Game Console</Text>
				</View>
				<View style={{ marginTop: 20 }}>
					{isLoading ? <ActivityIndicator size="large" color="#0000ff" /> : (data.map((item) => {
						return <RenderItem item={item} />
					})
					)}
				</View>
				<View style={{ flex: 1, padding: 30 }}></View>
			</ScrollView>
		</View>

	);
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: "white",
	},
});