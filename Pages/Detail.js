import Axios from 'axios';
import React, { useState, useEffect } from "react";
import {
	StyleSheet,
	Text,
	View,
	ScrollView,
	Image,
	ActivityIndicator,
	Dimensions
} from "react-native";

export default function Detail({ route, navigation }) {

	const [isLoading, setLoading] = useState(true);
	const [data, setData] = useState();
	const { key } = route.params;
	const { width } = Dimensions.get('window');


	useEffect(() => {
		Axios.get(`https://the-lazy-media-api.vercel.app/api/detail/${key}`)
			.then(({ data }) => {
				setData(data);
			})
			.catch((error) => console.error(error))
			.finally(() => setLoading(false));
	}, []);

	return (
		<View style={styles.container}>
			{isLoading ? <ActivityIndicator size="large" color="#0000ff" /> : (<ScrollView>
				<Text style={{ paddingLeft: 20, paddingRight: 20, fontSize: 20, fontWeight: 'bold' }}>{data["results"]["title"]} </Text>
				<Text style={{ paddingLeft: 20, paddingRight: 20, fontSize: 18, color: "#707070", marginTop: 10 }}>Penulis : {data["results"]["author"]} </Text>
				{
					data["results"]["content"].map((data, index) => {
						if (data.endsWith('.png') || data.endsWith('.jpg') || data.endsWith('.jpeg') || data.endsWith('.gif')) {
							return <Image source={{ uri: data }} resizeMode={'contain'} style={{ paddingLeft: 20, paddingRight: 20, width: width, height: '100%', maxHeight: 400, maxWidth: 400, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }} key={index} />
						} else if (data.includes('youtube.com') || data.includes('youtu.be')) {
							return <Text key={index}>{"\n"}</Text>;
						} else {
							return (
								<View key={index}>
									<Text style={{ fontSize: 18, fontWeight: '300', textAlign: 'justify', paddingLeft: 20, paddingRight: 20 }}>{data}</Text>
									<Text>{"\n"}</Text>
								</View>
							)
						}
					})
				}
			</ScrollView>)
			}
		</View >
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
		paddingTop: 20,
		resizeMode: 'contain'
	},
});
