import React from "react";
import { View, ScrollView, Image, Text, StyleSheet } from "react-native";

export default function AboutScreen() {
    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={{ padding: 20, borderRadius: 5, borderColor: "#8E8383", borderWidth: 0.6, marginTop: 20, marginBottom: 20 }}>
                    <Text style={{ fontSize: 34, fontWeight: "bold", alignSelf: "center", marginTop: 20 }}> Tentang Saya </Text>
                    <Image source={require('../assets/profile.png')} style={{ marginLeft: 18, alignSelf: "center", width: 200, height: 200, resizeMode: 'contain', marginTop: 35, borderRadius: 1000 }} />
                    <Text style={{ margin: (20, 14, 0, 12), fontSize: 30, fontWeight: "bold", alignSelf: "center", textAlign: 'center' }}>Cintara Surya Elidanto</Text>
                </View>
                <View style={{ margin: (54, 24, 24, 24) }}>
                    <View style={{ borderRadius: 5, borderColor: "#8E8383", borderWidth: 0.6, padding: 5 }}>
                        <Text style={{ fontSize: 30, fontWeight: "bold" }}>Media Sosial</Text>
                        <Text style={{ fontSize: 14, fontWeight: "200" }}>Temukan dan terhubung dengan saya melalui</Text>
                    </View>
                    <View style={{ borderRadius: 5, borderColor: "#8E8383", borderWidth: 0.6, padding: 5, flexDirection: "row" }}>
                        <View style={{ flex: 1, marginTop: 5, alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={require('../assets/fb.png')} style={{ alignSelf: "center", borderRadius: 1000, width: 70, height: 70 }} />
                            <Text style={{ textAlign: "center", marginTop: 5, fontSize: 14, fontWeight: "200", alignSelf: "center" }}>Cintara Surya Elidanto</Text>
                        </View>
                        <View style={{ flex: 1, marginTop: 5 }}>
                            <Image source={require('../assets/twitter.png')} style={{ alignSelf: "center", borderRadius: 1000, width: 70, height: 70 }} />
                            <Text style={{ textAlign: "center", marginTop: 5, fontSize: 14, fontWeight: "200", alignSelf: "center" }}>@suryaa6666</Text>
                        </View>
                        <View style={{ flex: 1, marginTop: 5 }}>
                            <Image source={require('../assets/instagram.png')} style={{ alignSelf: "center", borderRadius: 1000, width: 70, height: 70 }} />
                            <Text style={{ textAlign: "center", marginTop: 5, fontSize: 14, fontWeight: "200", alignSelf: "center" }}>@suryaelidanto1</Text>
                        </View>
                    </View>

                    <View style={{ marginTop: 25, borderRadius: 5, borderColor: "#8E8383", borderWidth: 0.6, padding: 5 }}>
                        <Text style={{ fontSize: 30, fontWeight: "bold" }}>Portofolio</Text>
                        <Text style={{ fontSize: 14, fontWeight: "200" }}>Project dan karya yang pernah saya kerjakan</Text>
                    </View>
                    <View style={{ borderRadius: 5, borderColor: "#8E8383", borderWidth: 0.6, padding: 5, flexDirection: "row" }}>
                        <View style={{ flex: 1, marginTop: 5 }}>
                            <Image source={require('../assets/gitlab.png')} style={{ alignSelf: "center", borderRadius: 1000, width: 70, height: 70 }} />
                            <Text style={{ textAlign: "center", marginTop: 5, fontSize: 14, fontWeight: "200", alignSelf: "center" }}>@suryaa6666</Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 25, borderRadius: 5, borderColor: "#8E8383", borderWidth: 0.6, padding: 5 }}>
                        <Text style={{ fontSize: 30, fontWeight: "bold" }}>Skill</Text>
                        <Text style={{ fontSize: 14, fontWeight: "200" }}>Kemampuan yang saya miliki</Text>
                    </View>
                    <View style={{ borderRadius: 5, borderColor: "#8E8383", borderWidth: 0.6, padding: 5, flexDirection: "row" }}>
                        <View style={{ flex: 1, marginTop: 5 }}>
                            <Image source={require('../assets/javascript.png')} style={{ alignSelf: "center", borderRadius: 1000, width: 70, height: 70 }} />
                            <Text style={{ textAlign: "center", marginTop: 5, fontSize: 14, fontWeight: "200", alignSelf: "center" }}>Basic JavaScript</Text>
                        </View>
                        <View style={{ flex: 1, marginTop: 5 }}>
                            <Image source={require('../assets/git.png')} style={{ alignSelf: "center", borderRadius: 1000, width: 70, height: 70 }} />
                            <Text style={{ textAlign: "center", marginTop: 5, fontSize: 14, fontWeight: "200", alignSelf: "center" }}>Basic Git</Text>
                        </View>
                        <View style={{ flex: 1, marginTop: 5 }}>
                            <Image source={require('../assets/react.png')} style={{ alignSelf: "center", borderRadius: 1000, width: 70, height: 70 }} />
                            <Text style={{ textAlign: "center", marginTop: 5, fontSize: 14, fontWeight: "200", alignSelf: "center" }}>Basic React Native</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1, padding: 30 }}></View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
        alignItems: "center",
    },
});
