import React from "react";
import {
	StyleSheet,
	Text,
	View,
	ScrollView,
	TouchableOpacity,
} from "react-native";

export default function Home({ route, navigation }) {
	const { email } = route.params;

	const Kategori = ({ judul }) => {
		return (
			<TouchableOpacity onPress={() => navigation.navigate(judul)} style={{ flex: 1, backgroundColor: 'black', padding: 10, borderRadius: 5, borderColor: '#8E8383', borderWidth: 0.6, marginTop: 10 }}>
				<Text style={{ textAlign: 'center', alignItems: 'center', color: 'white', fontSize: 35, fontWeight: '500' }}> {judul} </Text>
			</TouchableOpacity>
		);
	}

	return (
		<View style={styles.container}>
			<ScrollView>
				<View style={{ backgroundColor: 'black', padding: 50 }}>
					<Text style={{ color: 'white', fontSize: 34, fontWeight: 'bold' }}>Selamat Datang, {email}  </Text>
					<TouchableOpacity onPress={() => navigation.navigate("About")} style={{ flex: 1, marginTop: 30, borderRadius: 5, borderColor: 'white', borderWidth: 1, height: 40, alignSelf: 'center' }}>
						<Text style={{ textAlign: 'center', alignItems: 'center', justifyContent: 'center', color: 'white', fontSize: 24, fontWeight: 'bold' }}> Tentang Developer </Text>
					</TouchableOpacity>
				</View>
				<View style={{ margin: (50, 0, 0, 0) }}>
					<Text style={{ alignSelf: 'center', color: 'black', fontSize: 34, marginBottom: 33, marginTop: 29, fontWeight: 'bold' }}>Kategori Berita</Text>
					<Kategori judul={"Semua"} />
					<View style={{ flexDirection: 'row' }}>
						<Kategori judul={"Console"} />
						<Kategori judul={"Esports"} />
					</View>
					<Kategori judul={"GameNews"} />
					<Kategori judul={"LazyTalk"} />
					<View style={{ flexDirection: 'row' }}>
						<Kategori judul={"PC"} />
						<Kategori judul={"Reviews"} />
					</View>
					<View style={{ flex: 1, padding: 30 }}></View>
				</View>
			</ScrollView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
		alignItems: "center",
	},
});
