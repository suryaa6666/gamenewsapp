import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import "react-native-gesture-handler";
import Login from "./Login";
import Home from "./Home";
import Register from "./Register";
import AboutScreen from "./AboutScreen";
import Semua from "./Semua";
import Detail from "./Detail";
import Console from "./Console";
import Esports from "./Esports";
import GameNews from "./GameNews";
import LazyTalk from "./LazyTalk";
import PC from "./PC";
import Reviews from "./Reviews";


const Stack = createNativeStackNavigator();

export default function index() {
	return (
		<NavigationContainer>
			<Stack.Navigator initialRouteName="Login">
				<Stack.Screen
					name="Login"
					component={Login}
					options={{ headerShown: false }}
				/>
				<Stack.Screen
					name="Register"
					component={Register}
					options={{ headerShown: false }}
				/>
				<Stack.Screen
					name="Home"
					component={Home}
					options={{ headerTitle: "Daftar Barang" }}
				/>
				<Stack.Screen
					name="About"
					component={AboutScreen}
					options={{ headerTitle: "Tentang Developer" }}
				/>
				<Stack.Screen
					name="Semua"
					component={Semua}
					options={{ headerTitle: "Semua Berita" }}
				/>
				<Stack.Screen
					name="Console"
					component={Console}
					options={{ headerTitle: "Berita Console" }}
				/>
				<Stack.Screen
					name="Esports"
					component={Esports}
					options={{ headerTitle: "Berita Esports" }}
				/>
				<Stack.Screen
					name="GameNews"
					component={GameNews}
					options={{ headerTitle: "Berita GameNews" }}
				/>
				<Stack.Screen
					name="LazyTalk"
					component={LazyTalk}
					options={{ headerTitle: "Berita LazyTalk" }}
				/>
				<Stack.Screen
					name="PC"
					component={PC}
					options={{ headerTitle: "Berita PC" }}
				/>
				<Stack.Screen
					name="Reviews"
					component={Reviews}
					options={{ headerTitle: "Berita Reviews" }}
				/>
				<Stack.Screen
					name="Detail"
					component={Detail}
					options={{ headerTitle: "Detail" }}
				/>
			</Stack.Navigator>
		</NavigationContainer>
	);
}
