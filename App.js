import { StatusBar } from 'expo-status-bar';
import { StyleSheet } from 'react-native';
import Pages from './Pages';
import { getApps, initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyCRnYwvGUkZh0hMgPi6knxyE_Fg_mWTRZ4",
  authDomain: "gamenewsapp-a0ecc.firebaseapp.com",
  projectId: "gamenewsapp-a0ecc",
  storageBucket: "gamenewsapp-a0ecc.appspot.com",
  messagingSenderId: "338750868639",
  appId: "1:338750868639:web:c7ae4d47b5e5088a6466aa"
};

// Initialize Firebase
if (!getApps.length) {
  initializeApp(firebaseConfig);
}

export default function App() {
  return (
    <>
      <StatusBar translucent={false} />
      <Pages />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
